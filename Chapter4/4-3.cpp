#include <iostream>
#include <iomanip>


unsigned numDigits(const unsigned n) {
    if (n < 10) return 1;
    return 1 + numDigits(n / 10);
}


using std::cout;
using std::setw;
using std::endl;



int main() {
    int N = 1000;
    unsigned nDig = numDigits(N);
//    typedef vector<int>::size_type vec_sz;
//    vec_sz = 100;
    for(int i = 0; i<N; ++i) {
        cout << i+1 << setw(2*nDig+1)
            << (i+1)*(i+1)
            << endl;
    }
    cout << setw(0);


    return 0;
}
