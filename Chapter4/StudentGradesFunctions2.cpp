#include <algorithm>
#include <iomanip>
#include <ios>
#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>

using std::max;
using std::cin;
using std::cout;
using std::endl;
using std::setprecision;
using std::string;
using std::streamsize;
using std::sort;
using std::vector;
using std::istream;
using std::domain_error;

double median(vector<double> vec);
double grade(double midterm, double final, double homework) ;
double grade(double midterm, double final, const vector<double>& hw);
istream& read_hw(istream& in, vector<double>& hw);

struct StudentInfo {
    string name;
    double midterm, final;
    vector<double> homework;
}; // note the semicolon here

istream& read(istream& is, StudentInfo& s);
double grade(const StudentInfo& s);
bool compare(const StudentInfo& x, const StudentInfo& y);

int main()
{
    vector<StudentInfo> students;
    StudentInfo record;
    string::size_type maxlen = 0;

    // read and store all the records, find length of longest name
    while(read(cin, record)) {
        maxlen = max(maxlen, record.name.size());
        students.push_back(record);
    }

    //alphabetise the records
    sort(students.begin(), students.end(), compare);

    for (vector<StudentInfo>::size_type i = 0; i != students.size(); ++i) {

        // write the name, padded on the right to maxlen + 1 chars
        cout << students[i].name
                << string(maxlen+1-students[i].name.size(), ' ');

        // compute and write the grade
        try {
            double final_grade = grade(students[i]);
            streamsize prec = cout.precision();
            cout << setprecision(3) << final_grade
                    << setprecision(prec);
        } catch (domain_error e) {
            cout << e.what();
        }

        cout << endl;
    }

    return 0;

}


// compute the media of vector<double>
// note that calling this function copies the entire
// argument vector
double median(vector<double> vec) {
    typedef vector<double>::size_type vec_sz;

    vec_sz size = vec.size();
    if (size==0)
        throw domain_error("median of an empty vector");

    sort(vec.begin(), vec.end());

    vec_sz mid = size/2;

    return size % 2 == 0 ? (vec[mid] + vec[mid-1])/2 : vec[mid];
}

// compute a student's overall grade from midterm and final exams
// grades and homework grades
double grade(double midterm, double final, double homework) {
    return 0.2*midterm + 0.4*final + 0.4*homework;
}

// Compute a student's overall grade from midterm and final exam grades
// and a vector of homework grades.
// This function does not copy its argument, because median
// does so for us.
double grade(double midterm, double final, const vector<double>& hw) {
    if (hw.size() == 0)
        throw domain_error("student has done no homework");

    return grade(midterm, final, median(hw));
}

// read homework grades from an input stream into a vector<double>
istream& read_hw(istream& in, vector<double>& hw) {
    if (in) {
        // get rid of previous contents
        hw.clear();

        // read homework grades
        double x;
        while(in>>x)
            hw.push_back(x);

        // clear the stream so input will work for the next student
        in.clear();
    }
    return in;
}

istream& read(istream& is, StudentInfo& s) {
    // read and store the student's name and midterm and
    // final exam grades
    is >> s.name >> s.midterm >> s.final;

    read_hw(is, s.homework); // read and store all students homework grades.
    return is;
}

double grade(const StudentInfo& s) {
    return grade(s.midterm, s.final, s.homework);
}

bool compare(const StudentInfo& x, const StudentInfo& y) {
    return x.name < y.name;
}
