#include <iostream>
#include <vector>

using std::istream;
using std::vector;
using std::cout;
using std::cin;
using std::endl;

vector<double> read(istream&);
double average(vector<double>);

int main() {
        vector<double> nums;
        cout << "Enter numbers: ";
        nums = read(cin);
        cout << "The average is "
                << average(nums) << endl;


    return 0;
}

double average(vector<double> nums) {
    typedef vector<double>::size_type vec_sz;
    vec_sz size = nums.size();

    // computes the sum of all the numbers
    vec_sz sum = 0;
    for(double x : nums)
        sum += x;

    // computes the average
    return sum/size;
}

vector<double> read(istream& in) {
    vector<double> vec;

    // write input a vector named vec
    double x;
    while (in >> x)
        vec.push_back(x);

    return vec;
}
