#ifndef GUARD_median_h
#define GUARD_median_h

// source file for the median function
#include <algorithm>
#include <stdexcept>
#include <vector>



// compute the median of a vector<double>
// note that calling this function copies the entire argument vector
double median(std::vector<double> vec);

#endif
