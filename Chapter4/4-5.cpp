#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using std::istream;
using std::vector;
using std::string;
using std::cin;
using std::find;
using std::cout;
using std::endl;

vector<string> read(istream&);
vector<string> distinct_words(vector<string>);
vector<double>::size_type count_words(vector<string>, string);

int main() {

    typedef vector<double>::size_type vec_sz;
    vector<string> words;
    vector<string> dwords;

    // create the vector of words and the vector of distinct words
    words = read(cin);
    dwords = distinct_words(words);

    // find the size of the vector words
    vec_sz size = dwords.size();
    cout << "The total number of distinct words is  "
            << size << endl;

    vec_sz count;
    for(string s:dwords) {
        count = count_words(words, s);
        cout << "The word " << s << " appears "
                << count << " times." << endl;
        count = 0;
    }

    return 0;
}

vector<string> read(istream& in) {
    // Here we simply create our vector of words
    vector<string> words;
    string x;
    while(in>>x)
        words.push_back(x);

    return words;
}

vector<string> distinct_words(vector<string> words) {
    vector<string> dwords;
    for(string s:words) {
            if(find(dwords.begin(), dwords.end(), s) == dwords.end())
                dwords.push_back(s);
        }
    return dwords;
}

vector<double>::size_type count_words(vector<string> words, string word) {
    // for each word from the input stream, we count the number of times
    // it appears and save it in the variable count.
    vector<double>::size_type count = 0;

    for(string s:words) {
        if (s == word)
            ++count;
    }
    return count;
}
