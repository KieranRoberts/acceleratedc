#include <iostream>
#include <iomanip>

using std::cout;
using std::setw;
using std::endl;

int main() {
//    vector<int> numbers;
//    typedef vector<int>::size_type vec_sz;
//    vec_sz = 100;
    for(int i = 0; i<100; ++i) {
        cout << i+1 << setw(8)
            << (i+1)*(i+1)
            << endl;
    }
    cout << setw(0);


    return 0;
}
