#include <algorithm>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <list>
#include <stdexcept>
#include <string>
#include <vector>
#include "grade.h"
#include "Student_info.h"


using std::cin;
using std::cout;
using std::domain_error;
using std::endl;
using std::ifstream;
using std::list;
using std::max;
using std::setprecision;
using std::sort;
using std::streamsize;
using std::string;
using std::vector;

int main()
{
    vector<Student_info> students;
    Student_info record;
    string::size_type maxlen = 0;  // the length of the longest name
    ifstream in;
    in.open("grades.txt");

    // read and store all the students' data
    // Invariant:  students contains all the student records read so far
    // maxlen contains the length of the longest name in students

    while (read(in, record)) {
        // find length of longest name
        maxlen = max(maxlen, record.name.size());
        students.push_back(record);
    }

    in.close();

    // Alphabetise the student records
    sort(students.begin(), students.end(), compare);

    // We convert the vector students to a list and
    //separate the students who passed and failed
    list<Student_info> pass;
    std::copy(students.begin(), students.end(), std::back_inserter(pass));
    list<Student_info> fail = extract_fails(pass);

    // write the names and grades of students who passed
    //cout << "Students who passed: "<<endl;
    for (vector<Student_info>::size_type i = 0;
        i != students.size(); ++i) {

    // write the name, padded on the right to maxlen +1 characters
        cout << students[i].name
                << string(maxlen + 1 - students[i].name.size(), ' ');

        // compute and write the grade
        try {
            double final_grade = grade(students[i]);
            streamsize prec = cout.precision();
            cout << setprecision(3) << final_grade;
            cout << (
                    (fgrade(students[i]))
                    ? " FAIL"
                    : " PASS"
                    );
            cout << setprecision(prec);
            } catch (domain_error e) {
                cout << e.what();
        }
        cout << endl;
    }

    cout << endl << "The students who passed are ";
    for(list<Student_info>::iterator iter = pass.begin(); iter != pass.end(); ++iter)
        cout << (*iter).name << " ";
    cout << endl << "The students who failed are ";
    for(list<Student_info>::iterator iter = fail.begin(); iter != fail.end(); ++iter)
            cout << (*iter).name << " ";
    return 0;
}
