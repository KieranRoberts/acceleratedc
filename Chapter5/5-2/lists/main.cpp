#include <algorithm>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <list>
#include <stdexcept>
#include <string>
#include <vector>
#include "grade.h"
#include "Student_info.h"


using std::cout;
using std::clock;
using std::endl;
using std::ifstream;
using std::list;
using std::setprecision;
using std::sort;
using std::streamsize;
using std::string;
using std::vector;

double measure(clock_t, clock_t);

int main()
{
    // we initialise the timer
    clock_t start = clock();

    vector<Student_info> students;
    Student_info record;

    ifstream in;
    in.open("grades10000.txt");

    // read and store all the students' data
    // Invariant:  students contains all the student records read so fa

    while (read(in, record))
        students.push_back(record);

    in.close();

    // Alphabetise the student records
    sort(students.begin(), students.end(), compare);

    // We convert the vector students to a list and
    //separate the students who passed and failed
    list<Student_info> pass;
    std::copy(students.begin(), students.end(), std::back_inserter(pass));
    list<Student_info> fail = extract_fails(pass);

    // list the number of students who passed and failed.
    cout << students.size() << " passed." << endl;
    cout << fail.size() << " failed." << endl;

    streamsize prec = cout.precision();
    cout << "The program took " << setprecision(3)
            << measure(start, clock()) << " seconds to execute."
            << setprecision(prec) << endl;

    return 0;
}

double measure(clock_t start, clock_t stop) {
    return ((stop - start)/CLOCKS_PER_SEC);
}
