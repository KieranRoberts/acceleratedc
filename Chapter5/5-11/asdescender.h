#ifndef GUARD_asdescenders
#define GUARD_asdescenders

#include <string>
#include <vector>
#include <istream>

const std::vector<char> ascender = {'b','d','f','h','k','l','t'};
const std::vector<char> descender = {'g','j','p','q','y'};

struct word_info {
    std::string name;
    std::string::size_type length;
};


bool has_ascender(const std::string&);
bool has_descender(const std::string&);
bool is_word_without_asdescender(const std::string&);
std::istream& longest_word_without_asdescender(std::istream&, std::vector<word_info>&);
#endif

