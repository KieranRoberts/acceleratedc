#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "asdescender.h"

using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::cin;
using std::ifstream;
using std::max;
using std::ofstream;

int main() {
	vector<word_info> longest;
    ifstream in;
    in.open("dictionary.txt");
    ofstream out;
	out.open("goodwords.txt");

	while (longest_word_without_asdescender(in, longest));
    
    in.close();
    
    cout << "The longest word(s) have length " << longest[0].length << endl;
    cout << "The words are: " << endl;
    cout << longest.size() << endl;
    for (vector<word_info>::const_iterator it = longest.begin(); it != longest.end(); ++it)
        out << (*it).name << endl;
    
    out.close();
    return 0;
}
