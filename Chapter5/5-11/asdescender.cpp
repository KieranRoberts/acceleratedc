#include <algorithm>
#include <istream>
#include <iostream>
#include <string>
#include <vector>
#include "asdescender.h"

using std::string;
using std::find;
using std::istream;
using std::max;
using std::vector;
using std::cout;
using std::endl;

bool has_ascender(const string& s) {
	for (string::const_iterator it = s.begin(); it != s.end(); ++it) {
		if (find(ascender.begin(), ascender.end(), *it) != ascender.end())
		return true;
	}
	return false;
}

bool has_descender(const string& s) {
	for (string::const_iterator it = s.begin(); it != s.end(); ++it) {
		if (find(descender.begin(), descender.end(), *it) != descender.end())
		return true;
	}
	return false;
}

bool is_word_without_asdescender(const string& s) {
	if (!(has_ascender(s) || has_descender(s)))
		return true;
	else
		return false;
}

istream& longest_word_without_asdescender(istream& in, vector<word_info>& longest) {
	string s;
	word_info word;
	string::size_type maxlen = 0;
	
	while(in >> s) {
		if (is_word_without_asdescender(s)) {
			if (s.length() > maxlen) {
				longest.erase(longest.begin(), longest.end());
				maxlen = s.length();
				word.name = s;
				word.length = maxlen;
				longest.push_back(word);
			}
			else if (s.length() == maxlen) {
				word.name = s;
				word.length = maxlen;
				longest.push_back(word);
			}
			else
				continue;
		} 
		else
			continue;
	}
	return in;  
}
