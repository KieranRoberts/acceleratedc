#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>



using std::vector;
using std::string;
using std::getline;
using std::endl;
using std::cout;

vector<string> split(const string&);

int main() {

    std::ifstream names;
    names.open("names100000.txt");
    std::ofstream grades;
    grades.open("grades100000.txt");
    string line;
    vector<string> vecline;

    while(!names.eof()) {
        getline(names, line);
        vecline = split(line);
        grades << vecline[0] << " ";
        for(int i = 1; i<7; ++i)
            grades << rand() % 100 << " ";
        grades << endl;
    }

    names.close();
    grades.close();

    return 0;
}

vector<string> split(const string& s) {
    vector<string> ret;
    typedef string::size_type string_size;
    string_size i = 0;

    // invariant: we have processed the characters [original value of i, i)
    while (i != s.size()) {
        //ignore leading blanks
        // invariant: characters in range [original i, current i) are all spaces
        while (i != s.size() && isspace(s[i]))
            ++i;

        //find end of next word
        string_size j= i;
        //invariant: none of the characters in the range [original i, current j) is a space
        while (j != s.size() && !isspace(s[j]))
            j++;
        // if we found some non-whitespace characters
        if (i != j) {
            // copy from s start at i and taking j-i chars
            ret.push_back(s.substr(i,j-i));
            i = j;
        }

    }
    return ret;
}
