#ifndef GUARD_median_h
#define GUARD_median_h

// median.h
#include "driver.h"

double median(container<double>);

#endif
