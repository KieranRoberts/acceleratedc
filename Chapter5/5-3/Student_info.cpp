// source file for Student_info-related functions

#include "Student_info.h"
#include "grade.h"


using std::istream;
using std::vector;
using std::list;
bool compare(const Student_info& x, const Student_info& y)
{
     return x.name < y.name;
}

istream& read(istream& is, Student_info& s)
{
    // read and store the student's name and midterm and final exam grades
    is >> s.name >> s.midterm >> s.final;

    read_hw(is, s.homework);  // read and store all the student's homework grades
    return is;
}
istream& read_hw(istream& in, vector<double>& hw)
{
    if (in) {
    // get rid of previous content
    hw.clear();

    // read homework grades
    double x;
    while (in >> x)
        hw.push_back(x);

    // clear the stream so that input will work for the next student
    in.clear();
    }
    return in;
}

// predicate to determine wether a student passed or failed.

// first try: separate passing and failing students
 /* vector<Student_info> extract_fails(vector<Student_info>& students) {
    vector<Student_info> pass, fail;

    for (vector<Student_info>::size_type i = 0; i!= student.size(); ++i)
        if (fgrade(students[i]))
            fail.push_back(students[i]);
        else
            pass.push_back(students[i]);

    students = pass;
    return fail;
}*/

//second try: slower than the first function
/* vector<Student_info> extract_fails(vector<Student_info>& students){
    vector<Student_info> fail;
    vector<Student_info>::size_type i = 0;

    // invariant: elements [0,i) of students represent passing grades
    while (i != students.size()) {
        if(fgrade(students[i])) {
            fail.push_back(students[i]);
            students.erase(students.begin() + i);
        } else
            ++i;
    }
    return fail;
} */

// third try: iterators but no indexing, still potentially slow
vector<Student_info> extract_fails(vector<Student_info>& students) {
    vector<Student_info> fail;
    vector<Student_info>::iterator iter = students.begin();
    while (iter != students.end()) {
        if (fgrade(*iter)) {
            fail.push_back(*iter);
            iter = students.erase(iter);
        } else
            ++iter;
    }
    return fail;
}

// version 4: use list instead of vector
/* list<Student_info> extract_fails(list<Student_info>& students) {
    list<Student_info> fail;
    list<Student_info>::iterator iter = students.begin();

    while (iter != students.end()) {
        if (fgrade(*iter)) {
            fail.push_back(*iter);
            iter = students.erase(iter);
        } else
            ++iter;
    }
    return fail;
} */



