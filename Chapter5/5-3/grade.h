#ifndef GUARD_grade_h
#define GUARD_grade_h

// grade.h
#include <vector>
#include "Student_info.h"
#include "driver.h"

double grade(double, double, double);
double grade(double, double, const container<double>&);
double grade(const Student_info&);
bool fgrade(const Student_info&);


#endif
