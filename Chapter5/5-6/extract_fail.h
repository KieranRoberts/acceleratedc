#ifndef GUARD_extract_fail_h
#define GUARD_extract_fail_h

// extract_fail.h
#include <iostream>
#include <vector>
#include "Student_info.h"

std::vector<Student_info> extract_fails(std::vector<Student_info>&);
#endif
