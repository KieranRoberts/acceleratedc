#include <iostream>
#include <iterator>
#include <vector>
#include "extract_fail.h"
#include "grade.h"
#include "Student_info.h"

using std::vector;
using std::distance;


vector<Student_info> extract_fails(vector<Student_info>& students) {

    typedef vector<Student_info>::size_type vec_size;
    vec_size count = 0;

    vector<Student_info> fail;
    vector<Student_info>::iterator iter = students.begin();
    vector<Student_info>::iterator end_iter = students.end();

    vector<Student_info>::iterator tempiter;
    //vector<Student_info>::iterator divi = students.begin();
    //int diff;

    while (iter != end_iter) {
        if(fgrade(*iter)) {
            fail.push_back(*iter++);
        } else {
            tempiter = iter;
            students.insert(students.begin(), *iter);
            iter = ++tempiter;
            ++count;
        }
    }
    students.resize(count);
    return fail;
}
