#include <algorithm>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include "grade.h"
#include "Student_info.h"
#include "extract_fail.h"
//#include "median.h"

using std::cout;
using std::clock;
using std::endl;
using std::ifstream;
using std::setprecision;
using std::sort;
using std::streamsize;
using std::string;
using std::vector;

//double measure(clock_t, clock_t);

int main()
{
    // we initialise the timer
    //clock_t start = clock();

    vector<Student_info> students;
    Student_info record;

    ifstream in;
    in.open("grades10.txt");

    // read and store all the students' data
    // Invariant:  students contains all the student records read so far
    while (read(in, record))
        students.push_back(record);

    in.close();

    // We separate the students who passed and failed
    vector<Student_info> fail = extract_fails(students);

    // Students who passed:
    cout << "The students who passed are: ";
    for(vector<Student_info>::const_iterator it = students.begin(); it != students.end(); ++it)
        cout << (*it).name << " ";
    cout << endl;

    // Students who failed:
    cout << "The students who failed are: ";
    for(vector<Student_info>::const_iterator it = fail.begin(); it != fail.end(); ++it)
        cout << (*it).name << " ";
    cout << endl;

    return 0;
}

//double measure(clock_t start, clock_t stop) {
//    return ((stop - start)/CLOCKS_PER_SEC);
//}
