#include <iostream>
#include <fstream>
#include <string>
#include "palindrome.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;


int main() {
    ifstream dict;
    dict.open("dictionary.txt");
    ofstream palin;
    palin.open("palindromes.txt");

    string s;

    while(!dict.eof()) {
        getline(dict, s);
        if (isPalindrome(s))
            palin << s << endl;
    }
    return 0;
}



/* string s;
    s = "t";
    cout << s << (
            (isPalindrome(s))
            ? " is a palindrome"
            : " is not a palindrome"
            ) << endl;
    */
