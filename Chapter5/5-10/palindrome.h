#ifndef GUARD_palindrome_h
#define GUARD_palindrome_h

#include<string>

bool isPalindrome(std::string);
#endif
