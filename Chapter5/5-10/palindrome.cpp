#include <string>
#include "palindrome.h"

using std::string;

bool isPalindrome(string s) {
    bool flag = true;

    // We use the iterator and reverse iterator in string to compare
    // the first and last, second and last but one, etc.

    string::const_iterator iter = s.begin();
    string::reverse_iterator reviter = s.rbegin();

    // A disgusting while loop without a body.
    // The first two conditions check that we haven't run out of chars
    // The second condition checks that the two chars are equal, sets flag appropriately
    // AND increments both iterators

    while(iter != s.end() && reviter != s.rend() && (flag = (*iter++ == *reviter++)));

    return flag;

}

/* That body-less while loop is eqivalent to:
     while(iter != s.end() && reviter != s.rend() && flag) {
     *     if (*iter != *reviter)
     *         flag = false;
     *    ++iter;
     *     ++reviter;
     }
*/
