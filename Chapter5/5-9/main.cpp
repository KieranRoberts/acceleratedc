#include <iostream>
#include <string>
#include <vector>
#include "lucase_words.h"

using std::cout;
using std::endl;
using std::string;
using std::vector;

int main() {

    vector<string> words = {"Peter", "peTer", "peter", "A", "a", "123"};

    for(vector<string>::const_iterator it = words.begin(); it != words.end(); ++it) {
        cout << (*it) << (
            (IsLowerWord(*it))
            ? " is a lowercase word."
            : " is not a lowercase word."
                    ) << endl;
    }

    return 0;
}
