#include <string>
#include "lucase_words.h"

using std::string;
using std::istream;

bool IsLowerWord(const string& s) {
    bool flag = true;
    string::const_iterator iter = s.begin();
    while(iter != s.end() && ( flag = islower(*iter) ) ) {
        ++iter;
    }
    return flag;
}

istream& convertToLower(istream& in, string& s) {

    return in;
}
