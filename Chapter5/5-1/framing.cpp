#include <algorithm>
#include "framing.h"

using std::vector;
using std::max;
using std::string;


string::size_type width(const vector<string>& v)  {
    string::size_type maxlen = 0;
    for(vector<string>::size_type i = 0; i != v.size(); ++i)
        maxlen = max(maxlen, v[i].size());
    return maxlen;

}

vector<string> frame(const vector<string>& v) {

    // We rename the string::size_type to vec_sz since it will be used more than once.
    typedef string::size_type vec_sz;

    vector<string> ret;
    vec_sz maxlen = width(v);
    string border(maxlen + 4, '*');

    //write the top border
    ret.push_back(border);

    // write each interior row, bordered by an asterisk and a space
    for (vector<string>::size_type i = 0; i != v.size(); ++i) {
        vec_sz wsbuff = maxlen - v[i].size();

        // We add half of the necessary whitespace to each side of the string
        // so that it is centered.
        ret.push_back(
                (wsbuff % 2 == 0)
                ? "* " + string(wsbuff/2, ' ') + v[i] + string(wsbuff/2, ' ') + " *"
                : "* " + string(wsbuff/2, ' ') + v[i] + string(wsbuff/2+1, ' ') + " *"
        );
    }

    // write the bottom border
    ret.push_back(border);
    return ret;
}

vector<string> vcat(const vector<string>& top, const vector<string>& bottom) {
    //copy the top picture
    vector<string> ret = top;

    //copy the entire bottom picture
    for(vector<string>::const_iterator it = bottom.begin(); it != bottom.end(); ++it)
        ret.push_back(*it);
    return ret;
}

vector<string> hcat(const vector<string>& left, const vector<string>& right) {
    vector<string> ret;

    // add 1 to leave a space between pictures;
    string::size_type width1 = width(left) + 1;

    // indices to look at elements from left and right respectively
    vector<string>::size_type i = 0, j = 0;

    // continue until we've seen all rows from both pictures
    while (i != left.size() || j != right.size()) {
        // construct new string to hold characters from both pictures.
        string s;

        // copy a row from left-hand side, if there is one
        if (i != left.size())
            s = left[i++];

        // pad to full width
        s += string(width1 - s.size(), ' ');

        // copy a row from the right-hand side, if there is one
        if (j != right.size())
            s += right[j++];

        // add s to the picture we're creating
        ret.push_back(s);
    }
    return ret;
}
