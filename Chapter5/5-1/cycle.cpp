#include <vector>
#include <string>
#include "strtovec.h"

using std::vector;
using std::string;

vector<string> sentence_cycle(const vector<string>& vec) {
    vector<string> perm;
    vector<string> ret = strtovec(vec);
    //typedef vector<string>::size_type vec_sz;
    vector<string>::size_type size = vec.size();
    
    for(int i = 0; i<size; ++i) {
        string s0 = ret.begin();
        string temp;
        
        for(vector<string>::size_type i = ret.begin(); i!=ret.end(); ++i) {
            s[i] = s[int(i+1 % size)];
            temp += s[i];
        }
        perm.push_back(temp);
    }

    return perm;
}
