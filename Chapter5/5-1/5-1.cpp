#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include "strtovec.h"

using std::sort;
using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::tolower;
using std::transform;

int getmod(int val, int mod) {
      return ((val % mod) + mod) % mod; 
}

vector<string> sentence_cycle(const string& s) {
    vector<string> perm;
    vector<string> ret = split(s);
    //typedef vector<string>::size_type vec_sz;
    vector<string>::size_type size = ret.size();
    
    for(int i = 0; i<size; ++i) {
        string t0 = ret[0];
        string temp;
        
        for(vector<string>::size_type i = 0; i!=size-1; ++i) {
            ret[i] = ret[getmod(i+1,size)];
            temp += ret[i] + " ";
        }
        ret[size-1] = t0;
        temp += ret[size-1];
        perm.push_back(temp);
    }

    return perm;
}

vector<string> append_sentences(const vector<string>& vec) {
    vector<string> ret;
    vector<string> s;

    for(vector<string>::const_iterator it = vec.begin(); it != vec.end(); ++it) {
        s = sentence_cycle(*it);
        ret.insert(ret.begin(), s.begin(), s.end());
    }
    return ret;
}

int main() {
    vector<string> lines = {"he is the best", "she has been bad", "wow you"};
    
    vector<string> perm = append_sentences(lines);

    sort(perm.begin(), perm.end());

    for(vector<string>::size_type i = 0; i != perm.size(); ++i)
        cout << i+1 << ". "<< perm[i] << endl;
    
    return 0;
}
