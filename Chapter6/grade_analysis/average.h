#ifndef GUARD_average_h
#define GUARD_averge_h

#include <vector>
#include "Student_info.h"

double average(const std::vector<double>&);
double average_grade(const Student_info&);
#endif
