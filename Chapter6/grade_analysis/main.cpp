#include <algorithm>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <string>
#include <vector>

#include "analysis.h"
#include "grade.h"
#include "Student_info.h"
#include "average.h"

//#include "median.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::setprecision;
using std::sort;
using std::streamsize;
using std::string;
using std::vector;

int main()
{
    // students who did and didn't do all their homework
    vector<Student_info> did, didnt;

    // read the student records and partition them
    Student_info student;

    ifstream in;
    in.open("grades10.txt");

    // read and store all the students' data
    // Invariant:  students contains all the student records read so far
    while (read(in, student)) {
        if (did_all_hw(student))
            did.push_back(student);
        else
            didnt.push_back(student);
    }

    in.close();

    // verify that the analyses will show us something
    if (did.empty()) {
        cout << "No student did all the homework." << endl;
        return 1;
    }

    if (didnt.empty()) {
        cout << "Every student did all the homework." << endl;
    }

    // do the analyses

    ofstream out;
    out.open("analysis.txt");

    write_analysis(out, "median", median_analysis, did, didnt);
    write_analysis(out, "average", average_analysis, did, didnt);
    write_analysis(out, "median of homework turned in",
            optimistic_median_analysis, did, didnt);

    out.close();
    return 0;
}
