#include <algorithm>
#include <list>
#include <vector>

#include "Student_info.h"
#include "grade.h"
#include "extract_fails.h"

using std::list;
using std::vector;

}

// predicate to determine wether a student passed or failed.

// first try: separate passing and failing students
 /* vector<Student_info> extract_fails(vector<Student_info>& students) {
    vector<Student_info> pass, fail;

    for (vector<Student_info>::size_type i = 0; i!= student.size(); ++i)
        if (fgrade(students[i]))
            fail.push_back(students[i]);
        else
            pass.push_back(students[i]);

    students = pass;
    return fail;
}*/

//second try: slower than the first function
/* vector<Student_info> extract_fails(vector<Student_info>& students){
    vector<Student_info> fail;
    vector<Student_info>::size_type i = 0;

    // invariant: elements [0,i) of students represent passing grades
    while (i != students.size()) {
        if(fgrade(students[i])) {
            fail.push_back(students[i]);
            students.erase(students.begin() + i);
        } else
            ++i;
    }
    return fail;
} */

// third try: iterators but no indexing, still potentially slow
/* vector<Student_info> extract_fails(vector<Student_info>& students) {
    vector<Student_info> fail;
    vector<Student_info>::iterator iter = students.begin();
    while (iter != students.end()) {
        if (fgrade(*iter)) {
            fail.push_back(*iter);
            iter = students.erase(iter);
        } else
            ++iter;
    }
    return fail;
} */

// version 4: use list instead of vector
 list<Student_info> extract_fails(list<Student_info>& students) {
    list<Student_info> fail;
    list<Student_info>::iterator iter = students.begin();

    while (iter != students.end()) {
        if (fgrade(*iter)) {
            fail.push_back(*iter);
            iter = students.erase(iter);
        } else
            ++iter;
    }
    return fail;
}

 // Version 5: using more functions from <algorithms>
 /* vector<Student_info> extract_fails(vector<Student_info>& students) {
     vector<Student_info> fail;
     remove_copy_if(students.begin(), students.end(), back_inserter(fail), pgrade);
     students.erase(remove_if(students.begin(), students.end(), fgrade), students.end());
     return fail;
 }*/

 // Version 6: extract failed grades using partition function
 vector<Student_info> extract_fails(vector<Student_info>& students) {
     vector<Student_info>::iterator iter =
             stable_partition(students.begin(), students.end(), pgrade);
     vector<Student_info> fail(iter, students.end());
     students.erase(iter, students.end());
 }


