#include <fstream>
#include <iostream>
#include <vector>
#include <string>

#include "find_urls.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;

int main() {
    ifstream data;
    data.open("data.txt");
    vector<string> urls;
    vector<string> temp;
    string s;

    while (!data.eof()) {
        getline(data, s);
        temp = find_urls(s);
        urls.insert(urls.end(), temp.begin(), temp.end());
    }

    data.close();

    ofstream url;
    url.open("urls.txt");

    for(vector<string>::const_iterator it = urls.begin(); it != urls.end(); ++it)
        url << *it << endl;

    url.close();

    return 0;
}
