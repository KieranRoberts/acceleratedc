#include <iostream>
#include <string>
#include <vector>
#include "stringsplit.h"

using std::cout;
using std::endl;
using std::string;
using std::vector;

int main() {
    vector<string> vec;
    string s = "I am the greatest human being of all time";
    vec = split(s);

    for(vector<string>::const_iterator it = vec.begin(); it != vec.end(); ++it)
        cout << *it <<endl;

}
