#include <algorithm>
#include <string>
#include <vector>
#include "stringsplit.h"

using std::vector;
using std::string;
//using std::find_if;

// true if the argument is whitespace, false otherwise
bool space(char c) {
    return isspace(c);
}

// false if the argument is white space, true otherwise
bool not_space(char c) {
    return !isspace(c);
}

vector<string> split(const string& str) {
    typedef string::const_iterator iter;
    vector<string> ret;

    iter i = str.begin();
    while (i != str.end()) {

        // ignore leading blanks
        i = find_if(i, str.end(), not_space);

        // find end of next word
        iter j = find_if(i, str.end(), space);

        // copy the characters in [i,j)
        // string(i,j) is similar to subst(i,j) but operates on iterator and not indices.
        if (i != str.end())
            ret.push_back(string(i,j));
        i = j;
    }
    return ret;
}
