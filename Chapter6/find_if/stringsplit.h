#ifndef GUARD_stringsplit_h
#define GUARD_stringsplit_h

#include <string>
#include <vector>

bool not_space(char);
bool space(char);
std::vector<std::string> split(const std::string&);
#endif
