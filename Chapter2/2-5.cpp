#include <iostream>
#include <string>
// say what standard-library names we use
using std::cin;
using std::endl;
using std::cout;
//using std::string;
int main()
{
    // We first enter the length of one of the sides of the square or triangle
    cout << "Enter the length of one of the sides of the square or triangle: ";
    int len1;
    cin >> len1;
    const int len = len1;

    // Line break between input and output
    cout << endl;

    // We first construct the square

    cout << "The square:"<<endl;
    for(int l=0; l!=len; ++l){
        for(int m=0; m!= len; ++m){

            // We print a row of \*
            cout << "*";
        }

        // we print a line break to begin the next row
        cout << endl;
    }

    // A line break between square and triangle
    cout << endl;
    // We print the triangle
    cout << "The triangle: "<<endl;

    for(int l = 0; l!=len; ++l) {
        for(int m=0; m!=l+1; ++m){

            //We print the first row of \*
            cout << "*";
        }

        //We print a line break
        cout << endl;
    }
        return 0;
}

