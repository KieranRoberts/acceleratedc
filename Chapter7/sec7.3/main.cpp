#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "src/xref.h"
#include "src/strtovec.h"

using std::cout;
using std::cin;
using std::endl;
using std::map;
using std::string;
using std::vector;


int main() {
    // calling xref using split by default
    map<string, vector<int> > ret = xref(cin);
    
    // write the results
    
    for (map<string, vector<int> >::const_iterator it = ret.begin(); 
            it != ret.end(); ++it) {
        cout << it->first << " occurs on line(s): ";
        
        // followed by one or more lines
        vector<int>::const_iterator line_it = it->second.begin();
        cout << *line_it; // write first line number
        
        ++line_it;
        
        // write the rest of the line numbers, if any
       while( line_it != it->second.end()) {
            cout << ", " << *line_it;
            ++line_it;
        } 
        // write a line to separate each word
        cout << endl;
    }
    return 0;
}
