#ifndef GUARD_strtovec
#define GUARD_strtovec

// strtovec.h
#include <vector>
#include <string>

std::vector<std::string> split(const std::string&);
#endif