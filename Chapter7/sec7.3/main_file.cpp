#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <vector>
#include "xref.h"
#include "strtovec.h"

using std::endl;
using std::ifstream;
using std::ofstream;
using std::map;
using std::string;
using std::vector;


int main() {
    
    // open input file stream
    ifstream lines;
    lines.open("data/lines");
    
    // calling xref using split by default
    map<string, vector<int> > ret = xref(lines);
    
    // write the results
    // open output file stream
    ofstream ref;
    ref.open("data/ref");
    
    for (map<string, vector<int> >::const_iterator it = ret.begin(); 
            it != ret.end(); ++it) {
        ref << it->first << " occurs on line(s): ";
        
        // followed by one or more lines
        vector<int>::const_iterator line_it = it->second.begin();
        ref << *line_it; // write first line number
        
        ++line_it;
        
        // write the rest of the line numbers, if any
       while( line_it != it->second.end()) {
            ref << ", " << *line_it;
            ++line_it;
        } 
        // write a line to separate each word
        ref << endl;
    }
    return 0;
}
