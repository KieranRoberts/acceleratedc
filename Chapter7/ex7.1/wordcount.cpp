#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <fstream>

using std::ifstream;
using std::cout;
using std::endl;
using std::map;
using std::max;
using std::string;
using std::vector;

int main() {
    string s;
    map<string,int> counters; // store each word and associated counter
    string::size_type maxlen = 0;
    ifstream data;
    
    data.open("data/words");
    
    // read the input from a file, keeping track of each word and how often we see it.
    // record the length maxlen of the longest word.
    while (data >> s) {
        maxlen = max(maxlen, s.size());
        ++counters[s];
    }
    data.close();
    
    // new map whose keys are the frequency of words and values are vectors 
    // of strings which occur with that given key.
    
    map<int, vector<string> > wordsByFrequency;
    for(map<string,int>::const_iterator it = counters.begin(); it != counters.end(); ++it)
        wordsByFrequency[it->second].push_back(it->first);
        
    // Iterate through the new map whose keys are the frequencies.
    // For each frequency, sort the vector of words alphabetically.
    // Using the maxlen, align the frequecny of words in a column.
    
    for (map<int, vector<string> >::iterator it = wordsByFrequency.begin(); it != wordsByFrequency.end(); ++it) {
        sort(it->second.begin(), it->second.end());
        for(vector<string>::const_iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {
             cout << *it2 << string(maxlen-(*it2).size(), ' ') << "\t" << it->first << endl;
        }
    }

    return 0;
}
