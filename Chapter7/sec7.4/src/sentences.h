#ifndef GUARD_sentences
#define GUARD_sentences

#include <istream>
#include <map>
#include <string>
#include <vector>

typedef std::vector<std::string> Rule;
typedef std::vector<Rule> Rule_collection;
typedef std::map<std::string, Rule_collection> Grammar;

Grammar read_grammar(std::istream&);
std::vector<std::string> gen_sentence(const Grammar&);
void gen_aux(const Grammar&, const std::string&, std::vector<std::string>&);
bool bracketed(const std::string&);
#endif