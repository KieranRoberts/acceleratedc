#include <stdexcept>
#include <map>
#include <string>
#include <vector>
#include "sentences.h"
#include "strtovec.h"
#include "nrand.h"

using std::istream;
using std::logic_error;
using std::map;
using std::string;
using std::vector;

Grammar read_grammar(istream& in) {
    Grammar ret;
    string line;
    
    // read the input line by line.
    while(getline(in,line)) {
        // split the line into words delimited by whitespace
        vector<string> entry = split(line);
        if(!entry.empty())
            // use the category to store the associated rule
            ret[entry[0]].push_back(
                Rule(entry.begin()+1, entry.end()));
    }
    return ret;
}

vector<string> gen_sentence(const Grammar& g) {
    vector<string> ret;
    gen_aux(g,"<sentence>", ret);
    return ret;
}

bool bracketed(const string& s) {
    return s.size() > 1 && s[0] == '<' && s[s.size()-1] == '>';
}

void gen_aux(const Grammar& g, const string& word,
    vector<string>& ret) {
    if (!bracketed(word)) {
        ret.push_back(word);
    } else {
        // locate the rule that corresponds to the string word
        Grammar::const_iterator it = g.find(word);
        if (it == g.end())
            throw logic_error("empty rule");
        
        // fetch the set of possible rules
        const Rule_collection& c = it->second;
        
        // from which we select one at random
        const Rule& r = c[nrand(c.size())];
        
        // recursively expand the selected rule
        for (Rule::const_iterator i = r.begin(); i != r.end(); ++i)
            gen_aux(g,*i,ret);
    }
}

/* Let's see an example of how this may work applied to word = "<sentence>"
                                                                          
                                                                          
       +-----------+ <sentence> +----------------------------+            
       |              +      +                               |            
       v              |      |                               |            
      the             |      |                               |            
                      |     +----------------+               |            
                      v                       |              |            
           +---+ <noun-phrase>                |              |            
           |              +                   v              v            
           |              |                < verb>       <location>       
           v              |                   +              +            
      <adjective>         |                   |              |            
           +              |                   v              v            
           |              v                 sits       under the sky      
           |        <noun-phrase> +-+                                     
           v         +              |                                     
         large       |              |                                     
                     |              |                                     
                     v              v                                     
                <adjective>       <noun>                                  
                     +              +                                     
                     |              |                                     
                     |              |                                     
                     v              v                                     
                  absurd           dog                                    
                 
 
Here the arrows imply we are (recursively) applying gen_aux to the word above it.
                                                        
Ouput: the large absurd dog sits under the sky.  
*/                                                                                                                                               
