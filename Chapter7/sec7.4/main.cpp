#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "src/sentences.h"
#include "src/nrand.h"
#include "src/strtovec.h"

using std::endl;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;

int main(){
    int i=0;
    while(i<20){ 
        ++i;
        
        // open input streamed from file
        ifstream rules;
        rules.open("data/rules");
    
        // generate the sentences
        vector<string> sentence = gen_sentence(read_grammar(rules));

        // close the input stream
        rules.close();
    
        // open output stream to a file
        ofstream sentences;
        sentences.open("data/sentences", ofstream::out | ofstream::app);
    
        // write the first word, if any
        vector<string>::const_iterator it = sentence.begin();
        if(!sentence.empty()) {
            sentences << *it;
            ++it;
        }
    
        // write the rest of the words, each preceded by a space
        while (it != sentence.end()) {
            sentences << " " << *it;
            ++it;
        }
        sentences << endl;

        // close the output stream
        sentences.close();
    }
    return 0;
}