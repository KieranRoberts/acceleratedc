#include <algorithm>
#include <iomanip>
#include <ios>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::setprecision;
//using std::string;
using std::streamsize;
using std::sort;
using std::vector;

int main()
{
    // We ask the user to enter to the integers into a
    // vector.
    cout << "Enter the integers: ";

    vector<int> numberSet;
    int x;
    // invariant: numberSets contains all the chosen
    // so far
    while(cin >> x)
        numberSet.push_back(x);

    // check that at least one integer has been entered
    typedef vector<int>::size_type vec_sz;
    vec_sz size = numberSet.size();
    cout << size << endl;
    if (size==0) {
        cout << "You must give enter at least one"
                "integer into the set";
        return 1;
    }

    // sort the integers from smallest to largest
    sort(numberSet.begin(), numberSet.end());

    //compute the median, lower and upper quartile

    double lowerQ, upperQ;
    if (size % 4 == 0 || size % 4 == 2) {
        vec_sz mid1 = size/4;
        vec_sz mid2 = 3*size/4;
        lowerQ = numberSet[mid1];
        upperQ = numberSet[mid2+1];
    }
    else if (size % 4 == 1) {
        vec_sz n = (size-1)/4;
        cout << n << endl;
        lowerQ = 0.25*numberSet[n] + 0.75*numberSet[n+1];
        upperQ = 0.75*numberSet[3*n+1] + 0.25*numberSet[3*n+2];
    }
    else {
        vec_sz n = (size - 3)/4;
        cout << n << endl;
        lowerQ = 0.75*numberSet[n+1] + 0.25*numberSet[n+2];
        upperQ = 0.25*numberSet[3*n+2] + 0.75*numberSet[3*n+3];
    }

    streamsize prec = cout.precision();
    cout << "Lower quartile is "<< setprecision(3)
            << lowerQ
            << ". Upper quartile is " << upperQ
            << setprecision(prec) << endl;

    return 0;
}
