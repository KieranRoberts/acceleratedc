#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

int main()
{

    typedef vector<double>::size_type vec_sz;
    vec_sz NumberOfHW = 3;

    vector<string> students;
    vector<double> grades;
    string x;

    while(cout << "Enter your name: " && cin >> x) {
        // the student enters their name:
        students.push_back(x);
        cout << "Hello, " << x << "!" << endl;

        // the student now enters their midterm and final exam grades

        cout << "Please enter your midterm and final exam grades: " << endl;
        double midterm, final;
        cin >> midterm >> final;
        // The student x now enters his homework grades.
        cout << "Enter all " << NumberOfHW << " of your homework grades:" << endl;

        double y;
        double sum = 0;
        for(unsigned int i=0; i<NumberOfHW; ++i) {
            cin >> y;
            sum += y;
        }
        grades.push_back(0.2*midterm+0.4*final+0.4*sum/NumberOfHW);
    }
    cout << endl;
    vec_sz size = students.size();
    for(unsigned int i=0; i<size; ++i) {
        cout << "The student " << students[i] << " has final grade " << grades[i] << endl;
    }
    return 0;
}
