#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::find;

int main()
{
    // words entered into a vector one by one to
    //build a word list
    cout << "Please enter words one by one: " << endl;
    string x;
    vector<string> words;
    while(cin >> x)
        words.push_back(x);



    // check there is at least one string in words
    //typedef vector<string>::size_type vec_sz;
    //vec_sz size = words.size();
    if (words.size()==0) {
        cout << endl << "You must enter at least one word. "
                "Please try again." << endl;
        return 1;
    }

    // create a new vector that contains each distinct
    // words precisely once

    vector<string> dwords;
    for(string s:words) {
        if(find(dwords.begin(), dwords.end(), s) == dwords.end())
            dwords.push_back(s);
    }


     int count = 0;

     // For each word in our new list, we check to see how
     // many times it occurs in the old list and print it.
    for(string s:dwords) {
        for(string t:words) {
            if(s==t) {
                count++;
            }
        }
        cout << "The word " << s << " occurs " << count << " times." << endl;
        count = 0;
    }
    return 0;
}
