#include <algorithm>
#include <iomanip>
#include <ios>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

int main()
{
    // The user enters the words into a vector
    cout << "Enter at least one word" << endl;
    vector<string> words;
    string x;
    while(cin>>x)
        words.push_back(x);

    // if nothing is no user input we stop the the program

    if(words.size() == 0) {
        cout << "You must enter at least one word."
                "Please try again." << endl;
        return 1;
    }

    // declare longestWord, shortestWord and set them
    // to the length of the first word in the vector.

    string::size_type maxWord = words[0].length();
    string::size_type minWord = words[0].length();

    // So we iteratively keep track of the shortest
    // and longest word in the vector words.

    for(string s:words) {
        if(s.length() > maxWord)
            maxWord = s.length();
        if(s.length() < minWord)
            minWord = s.length();
    }

    // maxWord and minWord are within the largest
    // scope of the program and so printing them
    // here is fine.
    cout<< "The longest word has length " << maxWord << endl;
    cout<< "The shortest word has length " << minWord << endl;

    return 0;
}
