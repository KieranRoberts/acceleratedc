#include <iostream>

// Your indentation is bad. Be consistent, and preferably configure your
// editor to use 4 spaces, not an actual tab.
int main()
{
    /* This is a coment that extends over severa lines
     * because it uses /* and */ as its starting and ending delimiters */
    std::cout << "Does this work?" << std::endl;
     return 0;
}
